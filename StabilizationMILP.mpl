# Guillaume Moroz, February 2024
#
# Function: StabilizingPolynomiaMIlLP
#   Stable polynomial in an ideal using linear programming
#
# Parameters:
#   polynomials - a list of polynomials
#   orders    (optional)     - an integer or a list of integers; the maximal degrees of the cofactors
#                              (1 by default)
#   bound (optional)         - an integer; the coefficients of the cofactors are less or equal to bound.
#                              (99 by default)
#   maxiterations (optional) - an integer; the maximal number of Graeffe iterations
#                              (2 by default)
#   variables (optional)     - a list of names, the variables in the polynomials
#                              (lexicographic sort by default)
#
# Returns:
#   A stable polynomial *S* and a list of cofactor polynomials *cofactors*,
#   satisfying S = add(cofactors[i]*polynomials[i], #   i=1..nops(polynomials)).
#
# Example:
#   > StabilizingPolynomiaMIlLP([1-x*y, 2-x-y])
#         2              2
#   -166 x  + x y - 166 y  - x - y + 999, [333, 333 + 166 y + 166 x]
#
#   > StabilizingPolynomiaMIlLP([1-2*x*y, 5/3-x-y]); 
#   Error, (in StabilizingPolynomiaMIlLP) No stable polynomial found with the given parameters
#
#   > StabilizingPolynomiaMIlLP([1-2*x*y, 5/3-x-y], 2); 
#      2  2        3      2          2                  2                                 2        3        3
#   2 x  y  + 2 x y  + 2 x  y + 2 x y  + 4/3 x y - 4/3 y  - 2/3 x - 2/3 y + 2993/3 - 1/3 x  - 469 x  - 469 y ,
#   
#                2                             2                  2
#       [-x y - y  - 430 x - 430 y - 459, 469 x  + 389 x y + 469 y  + 782 x + 782 y + 874]
#
#   # The following example generates big intermediate values and triggers bug in
#   # the linear programming solver
#   > StabilizingPolynomiaMIlLP([(1-2*x*y)^2, 5/3-x-y], 2, 5)
#   Error, (in Optimization:-LPSolve) no feasible solution found
StabilizingPolynomialMILP := proc(polynomials ::list(polynom),
                         orders ::Or(integer, list(integer)) := 1,
                         bound ::integer := 99,
                         maxiterations ::integer := 2,
                         variables ::list(name) := sort([op(indets(polynomials))]),
                         {sparse ::truefalse := false,
                          nomilp::truefalse := false})
    local Ld, Ln, Lm, d, Lcr, f, LP, LU, n, lm, k, i, j, q, g, S;
    Ld := ifelse(type(orders, integer), [orders $ nops(polynomials)], orders);
    Lm := [seq(Monomials(variables, d), d in Ld)];
    n := add(nops(lm), lm in Lm);
    try 
        if sparse then
            LP := LinearProgramMaximizeDominantSparse(polynomials, Lm, bound);
        else
            LP := LinearProgramMaximizeDominant(polynomials, Lm, bound, nomilp);
        end if;
    catch:
        WARNING("milp solver failed, switching to continuous solver");
        LP := LinearProgramMaximizeDominant(polynomials, Lm, bound, true);
    end try;
    LU := [0 $ nops(polynomials)];
    k := 0;                                                   
    g := igcd(seq(round(LP[i]), i = 1..n));
    for i from 1 to nops(polynomials) do
        LU[i] := add(Lm[i][j]*round(LP[k+j])/max(1,g), j=1..nops(Lm[i]));
        k := k + nops(Lm[i]);
    end do;
    S := expand(add(LU[i]*polynomials[i], i=1..nops(polynomials)));
    if not IsStableCriterion(S, maxiterations) then
        WARNING("no polynomial with dominant constant found with the given parameters");
        return S, LU, false;
    end if;
    return S, LU, true;
end proc;

# Function: IsStableCriterion
#   Criterion to detect if a polynomial is stable
#
# Parameters:
#   f - a polynomial
#   maxiterations (optional) - an integer; the maximal number of Graeffe iterations
#                              (2 by default)
#   variables (optional)     - a list of names, the variables in the polynomials
#                              (lexicographic sort by default)
#
# Returns:
#   False if *f* has roots in the unit polydisk. If *f* has no root in the
#   polydisk and if *powers* is large enough, then returns true.
#
# Example:
#   > IsStableCriterion(3-x*y-x);
#   true
#   > IsStableCriterion((11/10+x)^2);
#   false
#   > IsStableCriterion((11/10+x)^2, 10);
#   true
IsStableCriterion := proc(f         ::polynom,
                          maxiterations ::integer := 2,
                          variables ::list(name) := sort([op(indets(f))]))
    local Zero, Gf, c, v, i, res;
    Zero := [seq(v=0, v in variables)];
    Gf := f;
    res := 2*abs(eval(Gf, Zero)) - add(abs(c), c in coeffs(expand(Gf)));
    i := 1;
    while res <= 0 and i < maxiterations do 
        Gf := GraeffeIteration(Gf, variables);
        res := 2*abs(eval(Gf, Zero)) - add(abs(c), c in coeffs(expand(Gf)));
        i := i+1
    end do;
    return evalb(res > 0);
end proc;


# Change of variables tools
GraeffeIteration := proc(f  ::polynom,
                         Lv ::list(name))
    local res,i,k,n,v;
    res := f;
    k := nops(Lv);
    for i from 1 to k do
        v := Lv[i];
        res := expand(res*eval(res, v=-v));
        res := expand(algsubs(v^2=v, res));
    end do;
    return res;
end proc;

# Linear programming tools for stability
Monomials := proc(Lv ::list(name),
                  d  ::integer)
    local n, Lv1, Lc, c, i, res;
    Lv1  := [op(Lv), 1];
    n   := nops(Lv1);
    Lc  := combinat:-composition(d+n, n);
    res := [seq(mul(Lv1[i]^(c[i]-1), i=1..n), c in Lc)];
end proc;

MacaulayMatrix := proc(Lf ::list(polynom),
                       Lm ::list(list(monomial)))
    local i, m, n, V, Lcm, lcm, B, L, M;
    V := [op(indets(Lf))];
    Lcm := [seq(seq([Vector([coeffs(expand(m*Lf[i]), V, 'lm')]), [lm]], m in Lm[i]), i=1..nops(Lf))];
    B := sort([op({1, seq(op(lcm[2]), lcm in Lcm)})]);
    m, n := nops(B), nops(Lcm);
    B := table([seq(B[i]=i, i=1..nops(B))]);
    M := Matrix(m,n);
    for i from 1 to n do
        L := [seq(B[m], m in Lcm[i][2])];
        M[L,i] := Lcm[i][1];
    end do;
    return M;
end proc;

#TODO: add option to remove integer constraint
LinearProgramMaximizeDominant := proc(Lf     ::list(polynom),
                                      Lm     ::list(list(monomial)),
                                      bound  ::integer,
                                      nomilp   ::truefalse )
    local B, N, M, m, n, d, Z, Br, Bs, Id, A, b, Aeq, beq, c, lb, ub, ni, res;
    M  := MacaulayMatrix(Lf, Lm);
    m  := LinearAlgebra:-RowDimension(M);
    n  := LinearAlgebra:-ColumnDimension(M);
    N  := LinearAlgebra:-MatrixNorm(M[2..m], infinity);
    d  := ilcm(seq(map(denom,convert(M[1..1],rational,exact))));
    B  := bound;
    Br := Vector(n, B);
    Bs := Vector(m-1, B * N);
    Id := LinearAlgebra:-IdentityMatrix(m-1, datatype=float);
    # Maximize the constant coefficients minus the sum of the absolute
    # values of the other coefficients
    A   := < < seq(-M[1]) | (0 $ m-1) >,
             <  M[2..m]   | -Id       >,
             < -M[2..m]   | -Id       > >;
    b   := < -1/d, (0 $ 2*m-2) >;
    c   := < seq(-M[1]), (1 $ m-1) >;
    lb := Vector([-Br, (0 $ m-1)]);
    ub := Vector([Br, Bs]);
    if nomilp then
        res := Optimization:-LPSolve(c, [A, b], [lb, ub]);
        if max(res[2]) < 1 then
            res[2] := res[2] / max(res[2]);
        end if;
    else
        res := Optimization:-LPSolve(c, [A, b], [lb, ub], integervariables=[seq(1..n)]);
    end if;
    return res[2];
end proc;

LinearProgramMaximizeDominantSparse := proc(Lf     ::list(polynom),
                                            Lm     ::list(list(monomial)),
                                            bound ::integer)
    # Maximize the constant coefficients minus the sum of the absolute
    # values of the other coefficients and minimize the number of monomials
    # using boolean variables in the middle column
    local B, N, M, m, n, d, Z, Br, Bs, Id, Zmm, Zmn, A, b, Aeq, beq, c, lb, ub, ni, res;
    M  := MacaulayMatrix(Lf, Lm);
    m  := LinearAlgebra:-RowDimension(M);
    n  := LinearAlgebra:-ColumnDimension(M);
    N  := LinearAlgebra:-MatrixNorm(M[2..m], infinity);
    d  := ilcm(seq(map(denom,convert(M[1..1],rational,exact))));
    B  := bound;
    Br := Vector(n, B);
    Bs := Vector(m-1, B * N);
    Id := LinearAlgebra:-IdentityMatrix(m-1, datatype=float);
    Zmm := LinearAlgebra:-ZeroMatrix(m-1,m-1);
    Zmn := LinearAlgebra:-ZeroMatrix(m-1,n);
    A   := < < seq(-M[1]) | (1 $ m-1) | (0 $ m-1) >,
             <  Zmn       |  Id       | -Id*B*N >,
             <  M[2..m]   | -Id       | Zmm     >,
             < -M[2..m]   | -Id       | Zmm     > >;
    b   := < -1/d, (0 $ 3*(m-1)) >;
    c   := < (0 $ n + m-1), (1 $ m-1)>;
    lb  := Vector([-Br, (0 $ 2*(m-1))]);
    ub  := Vector([ Br, Bs, (1 $ m-1)]);
    res := Optimization:-LPSolve(c, [A, b], [lb, ub],
                                 integervariables=[seq(1..n)],
                                 binaryvariables =[seq(n+m..n+2*(m-1))]);
    return res[2];
end proc;
